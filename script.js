let storage = []; // id тех сообщений, которые выведены на экран
const INTERVAL = 2000;
const urlChat = "http://localhost:8080/api/chat/message/";
const username = document.getElementById("name");
const text = document.getElementById("message");
const messageBox = document.getElementById("messagesArea");

function getValueInput() {
  return { username: username.value, text: text.value };
}

function clearMessage() {
  username.value = "";
  text.value = "";
}

function addMesageOnChat() {
  const messageData = getValueInput();

  if (messageData.username.trim() !== "" && messageData.text.trim() !== "") {
    axios.post(urlChat, messageData);
    //httpPost.call(data);
    getMessageFromServer();
    text.value = "";
  } else if (
    messageData.username.trim() === "" &&
    messageData.text.trim() === ""
  ) {
    alert("Введите имя и текст сообщения!");
  } else if (messageData.username.trim() === "") {
    alert("Вы не указали имя пользователя!");
  } else {
    alert("Вы не ввели текст сообщения");
  }
}

function sendMessageInBrowser(id, name, text) {
  if (messageBox !== null) messageBox.textContent = "";
  storage.reverse().forEach(function(item, i, storage) {
    createMessageinBrowser(item.username, item.text, item.id);
  });
}

function createButton() {
  const buttonsElement = document.createElement("div");
  const elementEdit = document.createElement("input");
  const elementDelete = document.createElement("input");

  elementEdit.classList.add("buttonEdit");
  elementEdit.setAttribute("type", "button");
  elementEdit.setAttribute("value", "Edit");

  elementDelete.classList.add("buttonDelete");
  elementDelete.setAttribute("type", "button");
  elementDelete.setAttribute("value", "x");

  buttonsElement.classList.add("buttons");
  buttonsElement.appendChild(elementEdit);
  buttonsElement.appendChild(elementDelete);

  return buttonsElement;
}

function createMessageinBrowser(username, text, id) {
  const button = createButton();
  const list = document.getElementById("messagesArea");
  const item = document.createElement("li");
  const textBox = document.createElement("div");
  const userpart = document.createElement("span");
  const messagepart = document.createElement("span");

  textBox.classList.add("messageText");
  item.classList.add("collection-item");
  messagepart.setAttribute("contenteditable", "false");

  userpart.textContent = username + ": ";
  messagepart.textContent = text;

  userpart.classList.add("bold");

  textBox.appendChild(userpart);
  textBox.appendChild(messagepart);
  item.appendChild(textBox);
  item.appendChild(button);
  list.appendChild(item);

  const buttonDelete = item.getElementsByClassName("buttonDelete")[0];
  buttonDelete.addEventListener("click", function() {
    const question = confirm("Вы уверены?");
    if (question === true) {
      fetch(urlChat + id, {
        method: "delete"
      });
      getMessageFromServer();
    }
  });

  const buttonEdit = item.getElementsByClassName("buttonEdit")[0];
  buttonEdit.addEventListener("click", function() {
    const messageEdit = buttonEdit.parentNode.previousSibling.lastChild;
    const blockMessage = buttonEdit.parentNode.previousSibling;

    if (messageEdit.getAttribute("contenteditable") === "false") {
      messageEdit.setAttribute("contenteditable", "true");
      blockMessage.setAttribute("class", "messageText changing");
      buttonEdit.setAttribute("value", "Save");
      stopInterval();
    } else if (messageEdit.getAttribute("contenteditable") === "true") {
      message = messageEdit.textContent;
      if (!message || message.trim() === "")
        return alert("Вы не ввели новое сообщение");

      messageEdit.setAttribute("contenteditable", "false");
      buttonEdit.setAttribute("value", "Edit");
      blockMessage.setAttribute("class", "messageText changed");
      axios.put(urlChat + id, { text: message });
      getMessageFromServer();
      startInterval();
    }
  });
}

function getMessageFromServer() {
  axios.get(urlChat).then(function(response) {
    response.data.forEach(function(item, i, response) {
      if (response.length !== storage.length) {
        storage = response.slice();
        sendMessageInBrowser(storage.username, storage.text, storage.id);
      }
      const bool =
        item.id === storage[storage.length - i - 1].id &&
        item.text === storage[storage.length - i - 1].text;
      if (!bool) {
        storage = response.slice();
        sendMessageInBrowser(storage.username, storage.text, storage.id);
      }
    });
  });
}
let timer = setInterval(getMessageFromServer, INTERVAL);

function startInterval() {
  timer = setInterval(getMessageFromServer, INTERVAL);
}

function stopInterval(){
  clearInterval(timer);
}

getMessageFromServer();

function scrollBottom(){
const messagesBlock = document.getElementById("messagesBlock");
messagesBlock.scrollTop = messagesBlock.scrollHeight;
}
